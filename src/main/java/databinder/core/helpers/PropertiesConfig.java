package databinder.core.helpers;

//import java.io.FileInputStream;
//import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Set;

public class PropertiesConfig {
	protected static Properties properties;
	
	public static void loadProperties() {
		
		properties = new Properties();
		
		try {
			
			ClassLoader loader = Thread.currentThread().getContextClassLoader();
			InputStream inputStream = loader.getResourceAsStream("project.properties");
		
			properties.load(inputStream);
			
		}catch(Exception e) {
			throw new IllegalStateException(e);  
		}
		
	}
	
	public static boolean hasProperty(String propertyName) {
		
		if(properties == null)
			loadProperties();
		
		return properties.containsKey(propertyName);
		
	}
	
	public static Properties getProperties() {
		
		if(properties == null) {
			loadProperties();
		}
				
		return properties;
		
	}
	
	public static Set<String> getPropertyNames() {
		
		if(properties == null) {
			loadProperties();
		}
		
		Set<String> names = properties.stringPropertyNames();
		
		return names;
	}
	
	public static String getProperty(String propertyName)  {
		
		if(properties == null)
			loadProperties();
		
		return properties.getProperty(propertyName);		
		
	}
	
	public static int getIntegerProperty(String propertyName)  {
		
		if(properties == null)
			loadProperties();
		
		return Integer.parseInt(properties.getProperty(propertyName));		
		
	}
	
	public static double getDoubleProperty(String propertyName) {
		
		if(properties == null)
			loadProperties();
		
		return Double.parseDouble(properties.getProperty(propertyName));		
		
	}
	
	public static boolean getBooleanProperty(String propertyName) {
		
		if(properties == null)
			loadProperties();
		
		return Boolean.parseBoolean(properties.getProperty(propertyName));	
		
	}
}
