package databinder.core.helpers;

import static databinder.core.helpers.PropertiesConfig.getProperty;
import static databinder.core.helpers.PropertiesConfig.hasProperty;
import static databinder.core.globals.Utils.*;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import databinder.core.globals.Constants;

public class DataTableHelper {

	private String filePath;
	private Workbook workbook;
	private Sheet currentSheet;
	private Row currentRow;
	private int currentRowIndex = 0;
	private Map<String, Integer> columnMap;
	private int rowCount;

	static Logger logger = Logger.getLogger(DataTableHelper.class);
	
	public int getRowCount() {
		return rowCount;
	}

	public int getCurrentRowIndex() {
		return currentRowIndex;
	}

	public DataTableHelper() throws EncryptedDocumentException, InvalidFormatException, IOException {

		String propertyName = "env.mapreader.map.dir";
		logger.debug("Trying to read property " + propertyName);

		columnMap = new HashMap<String, Integer>();

		if (hasProperty(propertyName)) {
			filePath = getProperty(propertyName);
		} else {
			logger.error("Property [" + propertyName + "] was not setted.");
		}

	}

	public Sheet setSheet(String name) throws Exception {

		name.trim();

		if (workbook == null)
			throw new Exception("The workbook of project configuration was not loaded.");

			currentSheet = workbook.getSheet(name);
		
		try {
			loadColumnNames();
			loadRowCount();
		} catch (Exception e) {
			throw e;
		}

		setFirstRow();
		return currentSheet;

	}

	public void openConfigWorkbook() throws EncryptedDocumentException, InvalidFormatException, IOException {

		if (filePath != null && !filePath.isEmpty()) {
			workbook = WorkbookFactory.create(new File(filePath));

			logger.info("Load WorkBook: [" + filePath + "]");
		} else {
			logger.info("Workbook path is null or is empty.");
		}

		workbook.close();

	}

	public void loadColumnNames() throws Exception {

		Row headerRow = currentSheet.getRow(0);
		int index = 0;

		columnMap.clear();

		for (Cell cell : headerRow) {

			if (columnMap.containsKey(cell.getStringCellValue())) {
				logger.error("Column map already contains name [" + cell.getStringCellValue() + "]");
				throw new Exception("Column map already contains name [" + cell.getStringCellValue() + "]");
			}

			columnMap.put(cell.getStringCellValue(), index++);

		}

	}
	
	

	public int getColumnByName(String columnName) throws Exception {

		if (columnMap.containsKey(columnName))
			return columnMap.get(columnName);
		else
			throw new Exception("Error on get column index for name [" + columnName + "]");

	}

	public String getCellStringValue(String columnName) throws Exception {
		Cell currentCell = currentRow.getCell(getColumnByName(columnName));
		return currentCell == null ? Constants.EMPTY_STRING : getCellValue(currentCell).toString();
	}


	public String getCellStringValue(int row, String columnName) throws Exception {
		return getCellValue(row, columnName).toString();
	}

	public boolean isEmpty() throws Exception {

		return currentRow == null || getCellValue(currentRow.getCell(0)).toString().isEmpty();

	}

	public void setNextRow() {
		currentRow = currentSheet.getRow(++currentRowIndex);
	}
	
	public void setFirstRow() {
		currentRowIndex = 0;
		setNextRow();
	}
	
	public Object getCellValue(int rowNumber, String columnName) throws Exception {
		Row row = currentSheet.getRow(rowNumber);
		return getCellValue(row.getCell(getColumnByName(columnName)));	
	}

	public Object getCellValue(String columnName) throws Exception {
		return getCellValue(currentRow.getCell(getColumnByName(columnName)));
	}


	public Object getCellValue(Cell cell) throws Exception {

		if(cell == null) return Constants.EMPTY_STRING;
		
		Object cellValue = null;
		CellType cellType = cell.getCellTypeEnum();

		if (cellType == CellType.STRING) {
			cellValue = cell.getStringCellValue();

		} else if (cellType == CellType.NUMERIC) {

			if (DateUtil.isCellDateFormatted(cell)) {
				cellValue = cell.getDateCellValue();
			} else {
				cellValue = cell.getNumericCellValue();
			}

		} else if (cellType == CellType.BOOLEAN) {
			cellValue = cell.getBooleanCellValue();

		} else if (cellType == CellType.FORMULA) {
			cellValue = cell.getCellFormula();

		} else if (cellType == CellType.BLANK) {
			cellValue = "";
		}

		return cellValue;

	}
	
	private void loadRowCount() throws Exception {
		
		setFirstRow();
		
		rowCount = 0;
		
		while(!isEmpty()) {
			++rowCount;
			setNextRow();
		}
		
		setFirstRow();
	}

//	public Object getCellValues(Cell cell) {
//		for (int rowIndex = 0; rowIndex <= sheet.getLastRowNum(); rowIndex++) {
//			  row = sheet.getRow(rowIndex);
//			  if (row != null) {
//			    Cell cell = row.getCell(colIndex);
//			    if (cell != null) {
//			      // Found column and there is value in the cell.
//			      cellValueMaybeNull = cell.getStringCellValue();
//			      // Do something with the cellValueMaybeNull here ...
//			    }
//	}

}
