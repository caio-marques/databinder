package databinder.core.writer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import static databinder.core.globals.Utils.*;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import databinder.core.globals.Utils;
import databinder.core.helpers.DataTableHelper;
import databinder.core.reader.MapReader;
import databinder.core.transfer.MatrixTO;

public class DataWriter {

	protected MapReader mapReader;
	protected DataTableHelper dt;
	protected Utils utils;
	private Workbook workbook;


	private Logger logger = Logger.getLogger(DataWriter.class);

	public DataWriter(MapReader mapReader) throws Exception {
		this.mapReader = mapReader;
	}

	public void writeData() throws Exception {
		dt = new DataTableHelper();
		utils = new Utils();

		logger.debug("Method loadData was invoked.");

		for (MatrixTO matrix : mapReader.getEntries().keySet()) {

			Object[][] data = matrix.getData();

			String workbookPath = matrix.getWorkBookPath();

			FileInputStream fileInput = new FileInputStream(new File(workbookPath));
			workbook = new XSSFWorkbook(fileInput);

			XSSFSheet sheet = (XSSFSheet) workbook.getSheet(matrix.getWorkSheetName());

			logger.info("Carregando a matrix [" + matrix.getName() + "] do arquivo [" + matrix.getWorkBookPath()
					+ "] da planilha [" + matrix.getWorkSheetName() + "]");

			for (int rowCount = 0; rowCount < data.length; rowCount++) {
				XSSFRow row = sheet.getRow(matrix.getRow() + rowCount -1);

				for (int colCount = 0; colCount < data[rowCount].length; colCount++) {

					if (row == null) {
						break;
					}

					XSSFCell cell = row.getCell(utils.columnNameToNumber(matrix.getColumn()) + colCount);

					if (cell == null) {
						break; // esse pontos de pausa é usado quando identifca celulas sem fomatação
					}

					else {
						Object captureData = data[rowCount][colCount];
						setCellValue(cell, captureData);
					}

					logger.debug("O valor: [" + data[rowCount][colCount] + "]");
					}

				fileInput.close();
				FileOutputStream fileOutput = new FileOutputStream(new File(workbookPath));
				workbook.write(fileOutput);
				fileOutput.close();
			}

		}
	}
}
