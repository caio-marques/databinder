package databinder.core.exceptions;

public class MapLoadException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1039754087507363855L;

	public MapLoadException() {
		super();
	}
	
	public MapLoadException(String message) {
		super(message);
	}
	
	public MapLoadException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public MapLoadException(Throwable cause) {
		super(cause);
	}

}
