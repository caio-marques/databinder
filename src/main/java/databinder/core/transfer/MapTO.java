package databinder.core.transfer;

//import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MapTO {
	
	protected Map<MatrixTO, List<VariableMapTO>> entries;

	public MapTO(Map<MatrixTO, List<VariableMapTO>> entries) {
		this.entries = entries;		
	}

	public Map<MatrixTO, List<VariableMapTO>> getEntries() {
		return entries;
	}
	
	public MapTO() {}
	

}
