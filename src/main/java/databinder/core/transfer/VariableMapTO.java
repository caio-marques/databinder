package databinder.core.transfer;

public class VariableMapTO {
	
	protected VariableTO variable;
	protected Integer row;
	protected Integer column; 
	
	public VariableMapTO(VariableTO variable, Integer row, Integer column) {
		this.variable = variable;
		this.row = row;
		this.column = column;
	}
	
	public VariableTO getVariable() {
		return variable;
	}
	
	public void setVariable(VariableTO variable) {
		this.variable = variable;
	}
	
	public Integer getRow() {
		return row;
	}
	
	public void setRow(int row) {
		this.row = row;
	}
	
	public Integer getColumn() {
		return column;
	}
	
	public void setColumn(int column) {
		this.column = column;
	}

}