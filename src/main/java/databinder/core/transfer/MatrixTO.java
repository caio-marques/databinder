package databinder.core.transfer;

public class MatrixTO {
	
	protected String id;
	protected String name;
	protected String workBookPath;
	protected String worksheetName;
	protected int row;
	protected String column;
	protected Object[][] data;
	
	public MatrixTO(String id, 
			        String name, 
			        String workBookPath, 
			        String worksheetName, 
			        int row,
			        String column) {
		super();
		this.id = id;
		this.name = name;
		this.workBookPath = workBookPath;
		this.worksheetName = worksheetName;
		this.row = row;
		this.column = column;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getWorkBookPath() {
		return workBookPath;
	}

	public void setWorkBookPath(String workBookPath) {
		this.workBookPath = workBookPath;
	}

	public String getWorkSheetName() {
		return worksheetName;
	}

	public void setWorkSheetName(String worksheetName) {
		this.worksheetName = worksheetName;
	}

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public String getColumn() {
		return column;
	}

	public void setColumn(String column) {
		this.column = column;
	}

	public Object[][] getData() {
		return data;
	}

	public void setData(Object[][] data) {
		this.data = data;
	}
	
}
