package databinder.core.transfer;

import java.util.List;

public class VariableTO {

	protected String id;
	protected String name;
	protected String workBookPath;
	protected String workSheetName;
	protected int row;
	protected String column;
	protected Object data;
	protected List<Object> dataList;

	public VariableTO() {
	}
	
	public VariableTO(String id, String name, String workBookName, String workSheetName, int row, String column) {

		super();
		this.id = id;
		this.name = name;
		this.workBookPath = workBookName;
		this.workSheetName = workSheetName;
		this.row = row;
		this.column = column;

	}



	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getWorkBookPath() {
		return workBookPath;
	}

	public void setWorkBookName(String workBookPath) {
		this.workBookPath = workBookPath;
	}

	public String getWorkSheetName() {
		return workSheetName;
	}

	public void setWorkSheetName(String workSheetName) {
		this.workSheetName = workSheetName;
	}

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public String getColumn() {
		return column;
	}

	public void setColumn(String column) {
		this.column = column;
	}
	
	public Object setData(Object data) {
		return this.data = data;
	}

	public Object getData() {
		return data;
	}

}
