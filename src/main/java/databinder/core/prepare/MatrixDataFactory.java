package databinder.core.prepare;

import static databinder.core.globals.Utils.verifyArray;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import databinder.core.globals.Utils;
import databinder.core.transfer.MatrixTO;
import databinder.core.transfer.VariableMapTO;
import databinder.core.transfer.VariableTO;

public class MatrixDataFactory {
	
	protected Map<MatrixTO, List<VariableMapTO>> entries;
	
	public MatrixDataFactory(Map<MatrixTO, List<VariableMapTO>> entries) {
		this.entries = entries;
	}
	
	public void prepareData() throws Exception {
		
		for(MatrixTO matrix : entries.keySet()) {
			
			int colSize = getMaxCol(entries.get(matrix));
			int rowSize = getMaxRow(entries.get(matrix));
			
			Object[][] data = new Object[rowSize][colSize];
			
			for(VariableMapTO variableMap : entries.get(matrix)) {
				
				Object dataVariable = variableMap.getVariable().getData();
				
				if(dataVariable instanceof ArrayList) {
					
					ArrayList values = (ArrayList)dataVariable;
						
					// Array de valores em linha
					if(variableMap.getVariable().getColumn() == null) {
			
						if(data[0].length < values.size() + variableMap.getColumn() -1) {
							data = Utils.resizeArray(data, 0, values.size() + variableMap.getColumn() -1);
						}
						
						for (int i=0; i < data[0].length; i++) {
							data[variableMap.getRow() - 1][variableMap.getColumn() + i - 1] = values.get(i);
						}
					
					// Array de valores em coluna
					} else {
						
						if(data.length < values.size() + variableMap.getRow() -1) {
							data = Utils.resizeArray(data, values.size() + variableMap.getRow() -1, 0);
						}
						
						for(int i=0; i < data.length; i++) {
							data[variableMap.getRow() + i - 1][variableMap.getColumn() - 1] = values.get(i);
						}
						
					}
					
				} else {
					data[variableMap.getRow() - 1 ][variableMap.getColumn() - 1] = dataVariable;
				}
			}
			
			matrix.setData(data);
		}		
	}
	
	public int getMaxRow(List<VariableMapTO> variablesMap) {
		
		int result = 0;
		
		for(VariableMapTO variableMap : variablesMap) {
			
			if(variableMap.getRow() > result)
				result = variableMap.getRow();
		}
		
		return result;
		
	}
	
	public int getMaxCol(List<VariableMapTO> variablesMap) {
		int result = 0;
		
		for(VariableMapTO variableMap : variablesMap) {
			
			if(variableMap.getColumn() > result)
				result = variableMap.getColumn();
		}
		
		return result;
		
	}

}
