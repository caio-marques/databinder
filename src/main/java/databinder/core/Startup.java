package databinder.core;

import org.apache.log4j.Logger;

import databinder.core.exceptions.MapLoadException;
import databinder.core.globals.Conversor;
import databinder.core.reader.DataReader;
import databinder.core.reader.MapReader;
import databinder.core.writer.DataWriter;

public class Startup {
	
	private static MapReader mapReader;
	private static DataReader dataReader;
	private static DataWriter dataWriter;
	private static Conversor conversor;
	
	private static Logger logger = Logger.getLogger(Startup.class);
	
	public static void main(String[] args) {
		
		try {
			conversor = new Conversor();
			conversor.csvToXLSX();
			
			mapReader = new MapReader();
			mapReader.load();;
			
			
			
			dataReader = new DataReader(mapReader);
			dataReader.loadData();
			
			dataWriter = new DataWriter(mapReader);
			dataWriter.writeData();
			
			logger.info("DONE");
			
		} catch (MapLoadException e) {
			logger.error("Error on loading map.", e);
		} catch (Exception e) {
 			logger.error("Generic error.", e);
		}
		
	}

}
