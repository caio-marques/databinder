package databinder.core.reader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import databinder.core.exceptions.MapLoadException;
import databinder.core.globals.Constants;
import databinder.core.helpers.DataTableHelper;
import databinder.core.transfer.MapTO;
import databinder.core.transfer.MatrixTO;
import databinder.core.transfer.VariableMapTO;
import databinder.core.transfer.VariableTO;

public class MapReader {

	//
	protected Map<String, VariableTO> variables;
	protected Map<String, MatrixTO> matrices;
	protected MapTO map;
	protected Map<MatrixTO, List<VariableMapTO>> entries;
	protected DataTableHelper dt;

	static Logger logger = Logger.getLogger(MapReader.class);

	public MapReader() {

		variables = new HashMap<String, VariableTO>();
		matrices = new HashMap<String, MatrixTO>();
		entries = new HashMap<MatrixTO, List<VariableMapTO>>();
		map = new MapTO(entries);

		try {
			dt = new DataTableHelper();
		} catch (Exception e) {
			logger.error("Error on initiating Datatable helper.", e);
		}

	}

	public Map<String, VariableTO> getVariables() {
		return Collections.unmodifiableMap(variables);
	}

	public Map<String, MatrixTO> getMatrices() {
		return Collections.unmodifiableMap(matrices);
	}

	public Map<MatrixTO, List<VariableMapTO>> getEntries() {
		return entries;
	}

	public MapTO load() throws Exception {

		logger.info("Method load was invoked.");

		try {
			dt.openConfigWorkbook();
		} catch (Exception e) {
			logger.error("Error on open config workbook.", e);
			throw new MapLoadException("Error on open config workbook.", e);
		}

		try {
			loadVariables();
		} catch (Exception e) {
			logger.error("Error on loading variables.", e);
			throw new MapLoadException("Error on loading variables.", e);
		}

		try {
			loadMatrix();
		} catch (Exception e) {
			logger.error("Error on loading matrix.", e);
			throw new MapLoadException("Error on loading matrix.", e);
		}

		try {
			loadMap();
		} catch (Exception e) {
			logger.error("Error on loading map.", e);
			throw new MapLoadException("Error on loading map.", e);
		}
		return map;

	}

	public void loadVariables() throws Exception {

		logger.debug("Method loadVariables was invoked.");

		dt.setSheet("MapeamentoEntrada");

		while (!dt.isEmpty()) {

			logger.debug("Obtendo valor da planilha [MapeamentoEntrada] na linha [" + dt.getCurrentRowIndex()
					+ "] da coluna [Id da Variável]");
			logger.debug("Resultado=[" + dt.getCellStringValue("Id da Variável") + "]");

			logger.debug("Obtendo valor da planilha [MapeamentoEntrada] na linha [" + dt.getCurrentRowIndex()
					+ "] da coluna [Nome da Variável]");
			logger.debug("Resultado=[" + dt.getCellStringValue("Nome da Variável") + "]");

			logger.debug("Obtendo valor da planilha [MapeamentoEntrada] na linha [" + dt.getCurrentRowIndex()
					+ "] da coluna [Pasta de Trabalho]");
			logger.debug("Resultado=[" + dt.getCellStringValue("Pasta de Trabalho") + "]");

			logger.debug("Obtendo valor da planilha [MapeamentoEntrada] na linha [" + dt.getCurrentRowIndex()
					+ "] da coluna [Planilha]");
			logger.debug("Resultado=[" + dt.getCellStringValue("Planilha") + "]");

			logger.debug("Obtendo valor da planilha [MapeamentoEntrada] na linha [" + dt.getCurrentRowIndex()
					+ "] da coluna [Linha]");
			logger.debug("Resultado=[" + dt.getCellStringValue("Linha") + "]");

			logger.debug("Obtendo valor da planilha [MapeamentoEntrada] na linha [" + dt.getCurrentRowIndex()
					+ "] da coluna [Coluna]");
			logger.debug("Resultado=[" + dt.getCellStringValue("Coluna") + "]");
			
			VariableTO variable;
			
			if (dt.getCellStringValue("Linha") == "") {
				
				variable = new VariableTO(dt.getCellStringValue("Id da Variável"),
						                  dt.getCellStringValue("Nome da Variável"), 
						                  dt.getCellStringValue("Pasta de Trabalho"),
						                  dt.getCellStringValue("Planilha"), 
						                  0, 
						                  dt.getCellStringValue("Coluna"));
			} 
			else if (dt.getCellStringValue("Coluna") == "") {
				
				variable = new VariableTO(dt.getCellStringValue("Id da Variável"),
						                  dt.getCellStringValue("Nome da Variável"), 
						                  dt.getCellStringValue("Pasta de Trabalho"),
						                  dt.getCellStringValue("Planilha"), 
						                  (int) Double.parseDouble(dt.getCellStringValue("Linha")),
						                  null);
			} 
			else {
				
				variable = new VariableTO(dt.getCellStringValue("Id da Variável"),
	                                      dt.getCellStringValue("Nome da Variável"), 
	                                      dt.getCellStringValue("Pasta de Trabalho"),
						                  dt.getCellStringValue("Planilha"), 
						                  (int) Double.parseDouble(dt.getCellStringValue("Linha")),
						                  dt.getCellStringValue("Coluna"));
			}

			if (variables.containsKey(variable.getId())) {
				logger.error("Variables already contains variable id.");
				throw new Exception("Variables already contains variable id.");
			}

			variables.put(variable.getId(), variable);

			dt.setNextRow();
		}

	}

	public void loadMatrix() throws Exception {

		logger.debug("Method loadMatrix was invoked.");

		dt.setSheet("MapeamentoSaida");

		while (!dt.isEmpty()) {

			logger.debug("Obtendo valor da planilha [MapeamentoEntrada] na linha [" + dt.getCurrentRowIndex()
					+ "] da coluna [Id da Matriz]");
			logger.debug("Resultado=[" + dt.getCellStringValue("Id da Matriz") + "]");

			logger.debug("Obtendo valor da planilha [MapeamentoEntrada] na linha [" + dt.getCurrentRowIndex()
					+ "] da coluna [Nome da Matriz]");
			logger.debug("Resultado=[" + dt.getCellStringValue("Nome da Matriz") + "]");

			logger.debug("Obtendo valor da planilha [MapeamentoEntrada] na linha [" + dt.getCurrentRowIndex()
					+ "] da coluna [Pasta de Trabalho]");
			logger.debug("Resultado=[" + dt.getCellStringValue("Pasta de Trabalho") + "]");

			logger.debug("Obtendo valor da planilha [MapeamentoEntrada] na linha [" + dt.getCurrentRowIndex()
					+ "] da coluna [Planilha]");
			logger.debug("Resultado=[" + dt.getCellStringValue("Planilha") + "]");

			logger.debug("Obtendo valor da planilha [MapeamentoEntrada] na linha [" + dt.getCurrentRowIndex()
					+ "] da coluna [Linha Inicial]");
			logger.debug("Resultado=[" + dt.getCellStringValue("Linha Inicial") + "]");

			logger.debug("Obtendo valor da planilha [MapeamentoEntrada] na linha [" + dt.getCurrentRowIndex()
					+ "] da coluna [Coluna Inicial]");
			logger.debug("Resultado=[" + dt.getCellStringValue("Coluna Inicial") + "]");

			MatrixTO matrix = new MatrixTO(dt.getCellStringValue("Id da Matriz"),
					dt.getCellStringValue("Nome da Matriz"), dt.getCellStringValue("Pasta de Trabalho"),
					dt.getCellStringValue("Planilha"), (int) Double.parseDouble(dt.getCellStringValue("Linha Inicial")),
					dt.getCellStringValue("Coluna Inicial"));

			if (matrices.containsKey(matrix.getId())) {
				logger.error("Matrixes already contains matrix id.");
				throw new Exception("Matrixes already contains matrix id.");
			}

			matrices.put(matrix.getId(), matrix);

			dt.setNextRow();
		}

	}

	public void loadMap() throws Exception {

		logger.debug("Method loadMap was invoked.");

		dt.setSheet("MapeamentoParametros");

		while (!dt.isEmpty()) {

			logger.debug("Obtendo valor da planilha [MapeamentoParametros] na linha [" + dt.getCurrentRowIndex()
					+ "] da coluna [Id da Matriz]");
			logger.debug("Resultado=[" + dt.getCellStringValue("Id da Matriz") + "]");

			String idMatrix = dt.getCellStringValue("Id da Matriz");

			if (!matrices.containsKey(idMatrix)) {

				logger.error("Id Matriz does not exist in matrixes.");
				throw new Exception("Id Matriz does not exist in matrixes.");

			} else {

				MatrixTO matrix = matrices.get(idMatrix);

				if (!entries.containsKey(matrix)) {
					List<VariableMapTO> variables = getVariablesForMatrix(idMatrix);
					entries.put(matrix, variables);
				}

			}

			dt.setNextRow();
		}

		logger.debug("Total entries loaded [" + entries.size() + "]");
	}

	public List<VariableMapTO> getVariablesForMatrix(String idMatrix) throws Exception {

		List<VariableMapTO> variablesForMatrix = new ArrayList<VariableMapTO>();

		for (int row = 1; row <= dt.getRowCount(); row++) {

			logger.debug(
					"Obtendo valor da planilha [MapeamentoParametros] na linha [" + row + "] da coluna [Id da Matriz]");
			logger.debug("Resultado=[" + dt.getCellStringValue(row, "Id da Matriz") + "]");

			String currentIdMatrix = dt.getCellStringValue(row, "Id da Matriz");

			if (currentIdMatrix == idMatrix) {

				logger.debug("Obtendo valor da planilha [MapeamentoParametros] na linha [" + dt.getCurrentRowIndex()
						+ "] da coluna [Id da Variável]");
				logger.debug("Resultado=[" + dt.getCellStringValue(row, "Id da Variável") + "]");

				logger.debug("Obtendo valor da planilha [MapeamentoParametros] na linha [" + dt.getCurrentRowIndex()
						+ "] da coluna [Linha]");
				logger.debug("Resultado=[" + dt.getCellStringValue(row, "Linha") + "]");

				logger.debug("Obtendo valor da planilha [MapeamentoParametros] na linha [" + dt.getCurrentRowIndex()
						+ "] da coluna [Coluna]");
				logger.debug("Resultado=[" + dt.getCellStringValue(row, "Coluna") + "]");

				String idVariable = dt.getCellStringValue(row, "Id da Variável"),
						linha = dt.getCellStringValue(row, "Linha"), coluna = dt.getCellStringValue(row, "Coluna");

				if (!variables.containsKey(idVariable)) {
					logger.error("Id Variable does not exist in variables");
					throw new Exception("Id Variable does not exist in variables.");
				} else {
					VariableMapTO variableMap = new VariableMapTO(variables.get(idVariable),
							linha == Constants.EMPTY_STRING ? null : (int) (Double.parseDouble(linha)),
							coluna == Constants.EMPTY_STRING ? null : (int) (Double.parseDouble(coluna)));
					variablesForMatrix.add(variableMap);
				}

			}
		}

		return variablesForMatrix;
	}
}
