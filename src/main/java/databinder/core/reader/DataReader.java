package databinder.core.reader;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import databinder.core.globals.Utils;
import databinder.core.helpers.DataTableHelper;
import databinder.core.prepare.MatrixDataFactory;
import databinder.core.transfer.VariableTO;

public class DataReader {

	protected static DataTableHelper dt;
	protected Utils utils;
	protected MapReader mapReader;
	private Workbook workbook;
	private Object data;
	
	private Logger logger = Logger.getLogger(DataReader.class);
	
	public DataReader() {}
	
	public DataReader(MapReader mapReader) throws Exception {

		this.mapReader = mapReader;

	}



	public void loadData() throws Exception {
		dt = new DataTableHelper();
		utils = new Utils();

		logger.debug("Method loadData was invoked.");

		for (VariableTO variable : mapReader.getVariables().values()) {
			String workbookPath = variable.getWorkBookPath();
			workbook = WorkbookFactory.create(new File(workbookPath));
			Sheet sheet = workbook.getSheet(variable.getWorkSheetName());

			logger.info("Carregando a variável [" + variable.getName() + "] do arquivo [" + variable.getWorkBookPath()
					+ "] da planilha [" + variable.getWorkSheetName() + "]");
		

			if (variable.getRow() == 0) {

				List<Object> colArray = new ArrayList<Object>();
				
				for (Row currentRow : sheet) {
					
					Cell cell = currentRow.getCell(utils.columnNameToNumber(variable.getColumn()));
					
					colArray.add(dt.getCellValue(cell));
				
				}
				
				variable.setData(colArray);
				data = variable.getData();
				logger.debug("Varaivel carregada: [" + data + "]");
				workbook.close();

			} else if (variable.getColumn() == null) {
				
				List<Object> rowArray = new ArrayList<Object>();
				
				Row currentRow = sheet.getRow(variable.getRow());
				
				for (Cell currentCell : currentRow) {
					rowArray.add(dt.getCellValue(currentCell));
					
				}
				variable.setData(rowArray);
				data = variable.getData();
				logger.debug("Varaivel carregada: [" + data + "]");
				workbook.close();

			} else {
				Row row = sheet.getRow(variable.getRow() - 1);
				Cell cell = row.getCell(utils.columnNameToNumber(variable.getColumn()));

				variable.setData(dt.getCellValue(cell));
				data = variable.getData();
				logger.debug("Varaivel carregada: [" + data + "]");
				workbook.close();
			}

		}

		new MatrixDataFactory(mapReader.getEntries()).prepareData();
	}
	
	
}
