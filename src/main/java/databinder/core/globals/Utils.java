package databinder.core.globals;



import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.util.CellReference;

public class Utils {

	public static boolean isNumeric(String s) {
		return s != null && s.matches("[-+]?\\d*\\.?\\d+");
	}

	public static boolean isArray(Object data) {
		return data.getClass().isArray();
	}
	
	
	
	public static void setCellValue(Cell cell, Object data) {

		if (data instanceof String) {
			cell.setCellValue((String) data);
		}

		else if (data instanceof Integer) {
			cell.setCellValue((Integer) data);
		}

		else if (data instanceof Boolean) {
			cell.setCellValue((Boolean) data);
		}

		else if (Utils.isNumeric(data.toString())) {
			cell.setCellValue((Double) data);
		}

	}
	
	

	public static Object getCellValue(Cell cell) {

		if (cell == null)
			return Constants.EMPTY_STRING;

		Object cellValue = null;
		CellType cellType = cell.getCellTypeEnum();

		if (cellType == CellType.STRING) {
			cellValue = cell.getStringCellValue();

		} else if (cellType == CellType.NUMERIC) {

			if (DateUtil.isCellDateFormatted(cell)) {
				cellValue = cell.getDateCellValue();
			} else {
				cellValue = cell.getNumericCellValue();
			}

		} else if (cellType == CellType.BOOLEAN) {
			cellValue = cell.getBooleanCellValue();

		} else if (cellType == CellType.FORMULA) {
			cellValue = cell.getCellFormula();

		} else if (cellType == CellType.BLANK) {
			cellValue = "";
		}

		return cellValue;

	}

	public String getColumnLetter(int index) {
		String columnLetter = CellReference.convertNumToColString(index);
		return columnLetter;
	}

	public int columnNameToNumber(String name) {
		int number = 0;
		for (int i = 0; i < name.length(); i++) {
			number = number * 26 + (name.charAt(i) - ('A' - 0));
		}
		return number;
	}

	public static boolean verifyArray(Object data) {
		String typeData = data.getClass().getTypeName();
		if (typeData == "java.util.ArrayList") {
			return true;
		}
		else {
			return false;
		}
	}
	
	public static Object[][] resizeArray(Object[][] data, int rows, int cols) throws Exception{
			
		Object[][] new_data = null;
		
		if (rows > 0 && rows > data.length)
			 new_data = new Object[rows][data[0].length];
		else if(cols > 0 && cols > data[0].length) {
			 new_data = new Object[data.length][cols];
		
			for(int linha=0; linha < data.length; linha ++) {	
				for(int coluna = 0; coluna  < data[linha].length; coluna ++) {
					new_data[linha][coluna] = data[linha][coluna];
				}
			}
		} else {
			throw new Exception("Invalid parameters for array resizing.");
		}
		
		return new_data;
	}

}
