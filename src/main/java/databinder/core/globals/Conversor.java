package databinder.core.globals;

import java.io.*;

import org.apache.log4j.Logger;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import static databinder.core.globals.Utils.*;
import static databinder.core.helpers.PropertiesConfig.*;

public class Conversor {
	static Logger logger = Logger.getLogger(Conversor.class);

//	public static void main(String[] args) {
//		if (args == null || args.length < 2) {
//			System.exit(0);
//		}
//		String sourceFileName = args[0];
//		String targetFileName = args[1];
//		csvToXLSX();
//	}

	public   void csvToXLSX() {

		String sourceFileName = "env.dir.csv";
		String targetFileName = "env.dir.xlsx";

		String sourceFilePath = null;
		String targetFilePath = null;
		if (hasProperty(sourceFileName)) {
			sourceFilePath = getProperty(sourceFileName);
		} else {
			logger.error("Source File(CSV): [" + sourceFileName + "] was not setted.");
		}

		if (hasProperty(targetFileName)) {
			targetFilePath = getProperty(targetFileName);
		} else {
			logger.error("Target File (XLSX): [" + targetFileName + "] was not setted.");
		}

		BufferedReader br = null;
		SXSSFWorkbook wb = null;
		FileOutputStream fileOutputStream = null;
		OutputStreamWriter osw = null;
		PrintWriter printWriter = null;

		Reader reader = null;
		try {
			String csvFileAddress = sourceFilePath;
			String xlsxFileAddress = targetFilePath;
			wb = new SXSSFWorkbook(100);
			wb.setCompressTempFiles(true); 
			Sheet sheet = wb.createSheet(getProperty("env.worksheet.xlsx"));
			String currentLine = null;
			int RowNum = 0;

			reader = new InputStreamReader(new FileInputStream(csvFileAddress), "utf-8");
			br = new BufferedReader(reader);

			while ((currentLine = br.readLine()) != null) {
				String str[] = currentLine.split(";");
				Row currentRow = sheet.createRow(RowNum);
				Double numeric = null;
				for (int i = 0; i < str.length; i++) {
					
					if (str[i].startsWith("=")) {
						str[i] = str[i].replaceAll("\"", "");
						str[i] = str[i].replaceAll("=", "");
						
						if(isNumeric(str[i])) {
							numeric = Double.parseDouble(str[i]);
							setCellValue(currentRow.createCell(i), numeric);
						}
						else {
							setCellValue(currentRow.createCell(i), str[i]);
						}
						
					} else if (str[i].startsWith("\"")) {
						str[i] = str[i].replaceAll("\"", "");

						if(isNumeric(str[i])) {
							numeric = Double.parseDouble(str[i]);
							setCellValue(currentRow.createCell(i), numeric);
						}
						else {
							setCellValue(currentRow.createCell(i), str[i]);
						}

					} else {
						str[i] = str[i].replaceAll("\"", "");

						if(isNumeric(str[i])) {
							numeric = Double.parseDouble(str[i]);
							setCellValue(currentRow.createCell(i), numeric);
						}
						else {
							setCellValue(currentRow.createCell(i), str[i]);
						}

					}

				}
				RowNum++;
			}

			fileOutputStream = new FileOutputStream(xlsxFileAddress);
			wb.write(fileOutputStream);
			osw = new OutputStreamWriter(fileOutputStream, "UTF-8");
			printWriter = new PrintWriter(osw);

			logger.debug("Conversion Done!");
		} catch (Exception ex) {
			logger.error(ex.getMessage() + "Exception in try");
		} finally {
			try {
				if (br != null)
					br.close();
				if (reader != null)
					reader.close();
				if (fileOutputStream != null)
					fileOutputStream.close();
				if (osw != null)
					osw.close();
				if (printWriter != null)
					printWriter.close();
			} catch (IOException e) {
				logger.error("Error to close resources.." + e.getMessage());
			}
		}
	}
}
